package il.co.ravtech.movieapp.details;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import il.co.ravtech.movieapp.R;
import il.co.ravtech.movieapp.model.MoviesContent;


public class DetailsActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM_POSITION = "init-position-data";
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPager = findViewById(R.id.details_vp_container);
        viewPager.setAdapter(mSectionsPagerAdapter);

        int startPosition = getIntent().getIntExtra(EXTRA_ITEM_POSITION, 0);
        viewPager.setCurrentItem(startPosition, false);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @Override
        public Fragment getItem(int position) {
            return MovieDetailsFragment.newInstance(MoviesContent.MOVIES.get(position));
        }

        @Override
        public int getCount() {
            return MoviesContent.MOVIES.size();
        }
    }
}
