package il.co.ravtech.movieapp.model;



import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity
public class VideoModel {

    @PrimaryKey
    private int movieId;

    private String id;

    private String key;

    public VideoModel() {

    }
    @Ignore
    public VideoModel(int movieId, String id, String key) {
        this.movieId = movieId;
        this.id = id;
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
