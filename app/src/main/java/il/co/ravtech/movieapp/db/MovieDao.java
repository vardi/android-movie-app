package il.co.ravtech.movieapp.db;



import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


import java.util.Collection;
import java.util.List;

import il.co.ravtech.movieapp.model.MovieModel;


@Dao
public interface MovieDao {

    @Query("SELECT * FROM MovieModel ORDER BY popularity DESC")
    List<MovieModel> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Collection<MovieModel> movies);

    @Query("DELETE FROM MovieModel")
    void deleteAll();

    @Query("SELECT * FROM movieModel WHERE name LIKE :name LIMIT 1")
    MovieModel findByName(String name);

    @Query("SELECT * FROM movieModel WHERE movieId IN (:movieIds)")
    List<MovieModel> loadAllByIds(String[] movieIds);

}
