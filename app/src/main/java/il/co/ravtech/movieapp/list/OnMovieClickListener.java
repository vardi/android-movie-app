package il.co.ravtech.movieapp.list;


public interface OnMovieClickListener {

    void onMovieClicked(int itemPosition);
}
